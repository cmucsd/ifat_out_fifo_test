`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN Lab
// Engineer: Christoph Maier
// 
// Create Date:    15:48:03 02/08/2013 
// Design Name: 
// Module Name:    rom 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies:
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rom(
    input clk,
    input [5:0] A,
    input AS,
    output [15:0] D,
    output reg DS= 0
    );

    reg [5:0] address= 0;
    reg [15:0] data [0:39];

    // parameter set generated from IFAT_UI/parameters/DAC/DAC_array_values_legend.csv
    // using parameter_extract.py script

    parameter
        VinMax_bottom= 6'd0,
        Vreset_bottom= 6'd1,
        Vthresh_bottom= 6'd2,
        Gcomp_bottom= 6'd3,
        Gleak_bottom= 6'd4,
        Eleak_bottom= 6'd5,
        dac6= 6'd6,
        dac7= 6'd7,
        VinMax_top= 6'd8,
        Vreset_top= 6'd9,
        Vthresh_top= 6'd10,
        Gcomp_top= 6'd11,
        Gleak_top= 6'd12,
        Eleak_top= 6'd13,
        dac14= 6'd14,
        dac15= 6'd15,
        Vtau_0_bottom= 6'd16,
        Vtau_1_bottom= 6'd17,
        Vtau_2_bottom= 6'd18,
        Vtau_3_bottom= 6'd19,
        Erev_0_bottom= 6'd20,
        Erev_1_bottom= 6'd21,
        Erev_2_bottom= 6'd22,
        Erev_3_bottom= 6'd23,
        Vtau_0_top= 6'd24,
        Vtau_1_top= 6'd25,
        Vtau_2_top= 6'd26,
        Vtau_3_top= 6'd27,
        Erev_0_top= 6'd28,
        Erev_1_top= 6'd29,
        Erev_2_top= 6'd30,
        Erev_3_top= 6'd31,
        Vbias= 6'd32,
        VpupArbit= 6'd33,
        VbnArbit= 6'd34,
        VbpArbit= 6'd35,
        VbpLogCell= 6'd36,
        VpupReq= 6'd37,
        Vspike= 6'd38,
        Vpdn= 6'd39;

    // parameter set generated from IFAT_UI/parameters/DAC/DAC_cfg_teddy01.csv
    // 1.4,0.4,0.138,0,0.45,0.35,0,0
    // 1.4,0.4,0.138,0,0.45,0.35,0,0
    // 0.1,0.1,0.1,0.1,0.3,0.5,0.3,0.5
    // 0.1,0.1,0.1,0.1,0.3,0.5,0.3,0.5
    // 1.1,1,0.5,1,1.1,0.7,1.3,0.5
    // using parameter_extract.py script

    initial
    begin
        data[VinMax_bottom]=16'd61166;
        data[Vreset_bottom]=16'd17476;
        data[Vthresh_bottom]=16'd6029;
        data[Gcomp_bottom]=16'd0;
        data[Gleak_bottom]=16'd19660;
        data[Eleak_bottom]=16'd15291;
        data[dac6]=16'd0;
        data[dac7]=16'd0;

        data[VinMax_top]=16'd61166;
        data[Vreset_top]=16'd17476;
        data[Vthresh_top]=16'd6029;
        data[Gcomp_top]=16'd0;
        data[Gleak_top]=16'd19660;
        data[Eleak_top]=16'd15291;
        data[dac14]=16'd0;
        data[dac15]=16'd0;

        data[Vtau_0_bottom]=16'd4369;
        data[Vtau_1_bottom]=16'd4369;
        data[Vtau_2_bottom]=16'd4369;
        data[Vtau_3_bottom]=16'd4369;
        data[Erev_0_bottom]=16'd13107;
        data[Erev_1_bottom]=16'd21845;
        data[Erev_2_bottom]=16'd13107;
        data[Erev_3_bottom]=16'd21845;

        data[Vtau_0_top]=16'd4369;
        data[Vtau_1_top]=16'd4369;
        data[Vtau_2_top]=16'd4369;
        data[Vtau_3_top]=16'd4369;
        data[Erev_0_top]=16'd13107;
        data[Erev_1_top]=16'd21845;
        data[Erev_2_top]=16'd13107;
        data[Erev_3_top]=16'd21845;

        data[Vbias]=16'd48059;
        data[VpupArbit]=16'd43690;
        data[VbnArbit]=16'd21845;
        data[VbpArbit]=16'd43690;
        data[VbpLogCell]=16'd48059;
        data[VpupReq]=16'd30583;
        data[Vspike]=16'd56797;
        data[Vpdn]=16'd21845;
    end

    always @(posedge clk)
    begin
        address <= AS? A : address;
        DS <= AS;
    end
    assign D= data[address];

endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN	Lab
// Engineer: Christoph Maier
// 
// Create Date:    16:18:26 04/09/2013 
// Design Name: 
// Module Name:    SynapseMonitor 
// Design Name:    HiAER-IFAT v.1.0
// Target Devices: 
// Tool versions: 
// Description: produces diagnostic text output from pulses to analog IFAT array
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SynapseMonitor(
  input [12:0] address,
  input pulse
  );

parameter block = -1;

wire [3:0] blockbits;

real pulsewidth;
reg [12:0] addr;

assign blockbits = block;

initial
begin
  pulsewidth = 0.0;
  addr = 13'bX;
end

always @(posedge pulse)
begin
  addr <= address;
  pulsewidth = -$realtime;
  $display("Time %d S D %h: Address event %h received by block %d, pulse started", $time, {blockbits,addr}, addr, blockbits);
end

always @(negedge pulse)
begin
  pulsewidth = pulsewidth + $realtime;
  if (address == addr)
    $display("Time %d S R %h %d: Address event %h received by block %d, pulse width %f", $time, {blockbits,addr}, pulsewidth, addr, blockbits, pulsewidth);  
  else
    $display("Time %d S E %h %h %d: Address conflict! Block %d, received pulse of width %f, address changed from %h to %h", 
             $time, {blockbits,addr}, {blockbits,address}, pulsewidth, blockbits, pulsewidth, addr, address);  
end

endmodule

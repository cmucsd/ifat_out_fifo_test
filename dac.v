`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN	Lab
// Engineer: Christoph Maier
// 
// Create Date:    19:34:30 08/09/2012 
// Design Name:    HiAER-IFAT v.1.0 test program
// Module Name:    dac 
// Project Name:   Neovision 2
// Target Devices: xc6slx45t-3fgg484
// Tool versions:  ISE 13.3
// Description:    Interface to the LTC2600 DAC daisy chain
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
// Here, I'm trying to build a self-contained functional block
// that prepares the data to stream to the DAC daisy chain
// and that is independent from Teddy's huge master spaghetti monster.
//
//////////////////////////////////////////////////////////////////////////////////
module dac(
    input clk,              // master clock
    input [15:0] data,      // parallel input data 
    input write,            // write command
    input rack,             // data read acknowlege
    output [5:0] address,   // data address
    output read,            // read data signal
    output busy,            // output indicating state machine is busy
    output reg ncs_ld= 1,   // control output to LTC2600: nCS/LD
    output sck,             // serial clock to LTC2600 daisy chain
    output sdo              // serial data out to LTC2600 daisy chain
    );

// If the state machine is not busy shifting out the previous datum, 
// an asserted "write" signal will initiate the shift out.
// Data that needs to be written will be read from a register array.
// The register array is addressed by a 6-bit address
// and returns 16-bit data.
// The read transaction is strobed by the read signal:
// at the rising edge of read, the address is valid.
// While rack is asserted, the 16-bit input data is assumed to be valid.

    parameter
        IDLE=       3'h0,
        UPDATE=     3'h1,
        READ=       3'h3,
        LATCH=      3'h7,
        SHIFT=      3'h6,
        WAIT=       3'h4;
    reg [2:0] state= IDLE;

    parameter   // LTC2600 commands
        DAC_WRITE=          4'h0,
        DAC_UPDATE=         4'h1,
        DAC_WRITE_ALLUP=    4'h2,
        DAC_WRITE_UPDATE=   4'h3,
        DAC_POWERDOWN=      4'h4,
        DAC_NOP=            4'hF;

    reg [2:0] dac_channel= 7;   // internal DAC channel
    reg [2:0] dac_chip= 4;      // DAC chip

    reg [31:0] serializer= 0;   // shift register

    wire request_data;          // request data from RAM
    wire have_data;             // data read from RAM
    wire send_long;             // send one long word of data to DACs
    wire parallel_load;         // parallel load serializer
    wire shift;                 // shift serializer
    wire serializer_busy;       // serializer is busy shifting

    wire [3:0] dac_command;

    assign address= {dac_chip, dac_channel};
    assign sdo= serializer[31];
    assign busy= (state != IDLE);
    assign request_data= (state == READ);
    assign send_long= (state == SHIFT);
    assign dac_command= (dac_channel == 3'o7)? DAC_WRITE_ALLUP : DAC_WRITE;

    always @(posedge clk)
    case (state)
        IDLE:
        begin
            if (write)
                state <= UPDATE;
            else
                state <= IDLE;

            dac_channel <= 3'o7;
            dac_chip <= 3'o4;
        end
        UPDATE:
        begin
            state <= READ;

            if (dac_chip == 3'o4)
            begin
                dac_chip <= 3'o0;
                dac_channel <= dac_channel+1;
                ncs_ld <= 1;
            end
            else
            begin
                dac_chip <= dac_chip+1;
            end
        end
        READ:
            state <= LATCH;
        LATCH:
        begin
            if (have_data)
            begin
                state <= SHIFT;

                ncs_ld <= 0;
            end
            else
                state <= LATCH;
        end
        SHIFT:
            state <= WAIT;
        WAIT:
        begin
            if (serializer_busy)
                state <= WAIT;
            else
                if (dac_chip == 3'o4 && dac_channel == 3'o7)
                begin
                    state <= IDLE;

                    ncs_ld <= 1;
                end
                else
                    state <= UPDATE;
        end
    endcase

    data_read read_sequencer(
        .clk(clk),
        .request(request_data),
        .data_valid(rack),
        .address_strobe(read),
        .parallel_load(parallel_load),
        .acknowledge(have_data)
    );

    shift_sequencer shift_long(
        .clk(clk),
        .send(send_long),
        .shift(shift),
        .sclk(sck),
        .busy(serializer_busy)
    );

    // shift register for one DAC datum
    always @(posedge clk)
    begin
        if (parallel_load)
            serializer <= {8'h00, dac_command, 1'b0, dac_channel, data};
        else if (shift)
            serializer <= serializer << 1;
        else
            serializer <= serializer;
    end
endmodule

// state machine for sequencing data read
//
// after a request (that also signals that the address is valid)
// assert address_strobe until data_valid
// assert parallel_load for one clock cycle
// assert acknowledge until request is cleared
//
module data_read(
    input clk,
    input request,
    input data_valid,
    output address_strobe,
    output parallel_load,
    output acknowledge
    );

    parameter
        IDLE= 2'h0,
        ADDR= 2'h1,
        LOAD= 2'h3,
        DONE= 2'h2;
    reg [1:0] state= IDLE;

    always @(posedge clk)
    begin
        case (state)
        IDLE:
            if (request)
                state <= ADDR;
            else 
                state <= IDLE;
        ADDR:
            if (data_valid)
                state <= LOAD;
            else
                state <= ADDR;
        LOAD:
            state <= DONE;
        DONE:
            if (request)
                state <= DONE;
            else
                state <= IDLE;
        endcase
    end

    assign address_strobe= (state == ADDR);
    assign parallel_load= (state == LOAD);
    assign acknowledge= (state == DONE);
endmodule

// state machine for shifting 32 bits of serial data
module shift_sequencer(
    input clk,
    input send,
    output shift,
    output sclk,
    output busy
    );
    reg [4:0] bitcount= 5'h00;
    wire done;
    wire delay;         // activate shift clock prescaler

    clk_divider div(
        .clkin(clk),
        .done(done),
        .run(delay)
    );

    parameter 
        IDLE=       2'o0,
        SHIFT_DATA= 2'o1,
        WAIT_L=     2'o3,
        WAIT_H=     2'o2;
    reg [1:0] state= IDLE;

    always @(posedge clk)
    begin
        case (state)
            IDLE:
            begin
                if (send)
                    state <= SHIFT_DATA;
                else
                    state <= IDLE;

                bitcount <= 5'h00;
            end
            SHIFT_DATA:
            begin
                state <= WAIT_L;

                bitcount <= bitcount - 1;
            end
            WAIT_L:
                if (done)
                    state <= WAIT_H;
                else
                    state <= WAIT_L;
            WAIT_H:
                if (done)
                    if (bitcount == 0)
                        state <= IDLE;
                    else
                        state <= SHIFT_DATA;
                else 
                    state <= WAIT_H;
        endcase
    end

    assign delay= (state == SHIFT_DATA || state == WAIT_L || state == WAIT_H);
    assign busy= (state != IDLE);
    assign sclk= (state == WAIT_H);
    assign shift= (state == SHIFT_DATA && bitcount != 0);
endmodule

// wait state counter for DAC serial clock
// Count while run is active. 
// This permits waiting for other processes to complete.
module clk_divider(
    input clkin,
    input run,
    output done
    );
//
// Divide clock by factor 2*prescale
// 
    parameter
        prescale= 4'hF;

    reg [3:0] count= 4'h0;

    assign done= (count == 0);

    always @(posedge clkin)
    begin
        if (done)
            if (run)
                count <= prescale;
            else
                count <= 0;
        else
            count <= count-1;
    end
endmodule

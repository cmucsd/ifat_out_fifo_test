`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN Lab
// Engineer: Christoph Maier
// 
// Create Date:    21:42:34 02/21/2013 
// Design Name: 
// Module Name:    ifat_synapse_delay_fifo 
// Project Name:   Neovision2 IFAT
// Target Devices: HiAER-IFAT Level 0 Spartan 6
// Tool versions:  XISE 13.3
// Description: 16-entry deep FIFO with delay logic 
//              to prevent internal IFAT logic from freezing up
//              as address events are sent to the IFAT.
//              One FIFO is required for each 2^11 neuron array.
// Dependencies:  ifat_synapse_scheduler.v
//                FIFO19x16.xco
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ifat_synapse_delay_fifo(
  input [3:0] top_address,  // top 4 bits of address (because there are 16 FIFOs per port)
  input reset,
  input inclk,  // input clock for FIFO (active rising edge)
  output full,  // FIFO full
  input [18:0] data_in, // 13 address bits + 6 synaptic weight bits
  input write_enable, // FIFO write enable
  input outclk, // output clock for IFAT interface
  output [22:0] ifat_in,  // 17 address bits + 6 synaptic weight bits
  output ifat_req,  // AER request
  input ifat_ack,   // AER acknowledge
  output busreq,    // bus request for IFAT port (to arbiter)
  input busgrant    // bus grant for IFAT port (from arbiter)
  );

  wire fifo_readclk;
  wire fifo_empty;
  wire fifo_valid;
  wire fifo_read;
  wire [18:0] fifo_out;

  FIFO19x16 synapse_fifo (
    .rst(reset), // input
    .full(full), // output full
    .wr_clk(inclk), // input wr_clk
    .wr_en(write_enable), // input wr_en
    .din(data_in), // input [18 : 0] din
    .empty(fifo_empty), // output empty
    .valid(fifo_valid), // output valid
    .rd_clk(fifo_readclk), // input rd_clk
    .rd_en(fifo_read), // input rd_en
    .dout(fifo_out) // output [18 : 0] dout
  );

  ifat_synapse_scheduler synapse_scheduler(
    .clk(outclk), 
    .reset(reset),
    .top_address(top_address),
    .fifo_out(fifo_out), 
    .fifo_readclk(fifo_readclk), 
    .fifo_empty(fifo_empty), 
    .fifo_valid(fifo_valid),
    .fifo_read(fifo_read), 
    .ifat_in(ifat_in), 
    .ifat_req(ifat_req), 
    .ifat_ack(ifat_ack), 
    .busreq(busreq), 
    .busgrant(busgrant)
  );
endmodule

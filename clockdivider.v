`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:     UCSD ISN lab 
// Engineer:    Christoph Maier
// 
// Create Date: 23:03:46 10/31/2012 
// Design Name: BFSKtest
// Module Name: clockdivider 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: clock divider that can also not divide
//
// Dependencies: clkvardiv.v
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module clockdivider(
    input [5:0] divisor,
    input clkin,
    input reset,
    output clkout
    );

    clkvardiv #(6) bitdiv (.divisor(divisor), .clkin(clkin), .reset(reset), .clkout(divclk)); 
    assign clkout = (divisor == 6'd1) ? clkin : divclk;

endmodule

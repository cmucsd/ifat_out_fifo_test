`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN Lab
// Engineer: Christoph Maier
// 
// Create Date:    16:36:16 02/08/2013 
// Design Name: 
// Module Name:    ifat_synapse_scheduler 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Backend to a Xilinx IP FIFO that delays the next output 
//              until the IFAT FIFO had enough time to transmit the pulse
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ifat_synapse_scheduler(
  input clk,
  input reset,
  // hardwired FIFO MSNibble address
  input [3:0] top_address,
  // FIFO AER output
  input [18:0] fifo_out,
  // FIFO handshake signals
  output fifo_readclk,
  input fifo_empty,
  input fifo_valid,
  output reg fifo_read,
  // bus to IFAT EXT_IN (need to be tri-state and controlled via arbiter)
  output [22:0] ifat_in,
  // IFAT handshake signals (need to get multiplexed by arbiter)
  output ifat_req,
  input ifat_ack,
  // interface signals to arbiter
  output reg busreq,
  input busgrant
  );

parameter [5:0] extra_cycles= 10'h0; // add extra wait states for count

reg [18:0] address_event;
reg aer_request;

reg [6:0] count;  // delay counter

wire [6:0] delay;     // initial delay
wire data_available;  // data is available from FIFO
wire ready_to_send;   // data is ready to send
wire sent;            // data has been sent

wire bus_request;
wire bus_enable;

wire aer_launch;

wire zero_count;  // counter is at zero

assign ifat_in = bus_enable ? {top_address, address_event} : 23'b0;
assign ifat_req = bus_enable ? aer_request : 1'b0;

assign fifo_readclk = clk;

assign bus_enable = busreq & busgrant;


reg [2:0] state;
parameter [2:0] // Grey counter
  IDLE= 3'h0,
  REQ=  3'h1,
  READ= 3'h3,
  SEND= 3'h7,
  WAIT= 3'h5;

// state machine
assign data_available = ~fifo_empty; 
assign ready_to_send = bus_enable & ~ifat_ack;
assign sent = bus_enable & ifat_ack;
assign zero_count = (0 == count);

always @(posedge clk or posedge reset)
begin
  if (reset)
    state <= IDLE;
  else
  begin
    case (state)
    IDLE:
      state <= data_available ? REQ : IDLE;
    REQ:
      state <= READ;
    READ:
      state <= ready_to_send ? SEND : READ;
    SEND:
      state <= sent ? WAIT : SEND;
    WAIT:
      state <= zero_count ? IDLE : WAIT;
    default:
      state <= IDLE;
    endcase
  end
end

// FIFO read
always @(posedge clk or posedge reset)
begin
  if (reset)
    fifo_read <= 1'b0;
  else
    fifo_read <= (IDLE == state) && data_available;
end

// down counter
assign delay = address_event[5:0] + extra_cycles;
assign aer_launch = (READ == state) && ready_to_send;

always @(posedge clk or posedge reset)
begin
  if (reset)
    count <= 6'h0;
  else
  begin
    if (aer_launch)
      count <= delay;
    else
      count <= zero_count ? count : count-1;
  end
end

// bus arbitration handshake
assign bus_request = (READ == state);

always @(posedge clk or posedge reset)
begin
  if (reset)
    busreq <= 1'b0;
  else if (bus_request)
    busreq <= 1'b1;
  else if (sent)
    busreq <= 1'b0;
  else
    busreq <= busreq;
end

// AER handshake
always @(posedge clk or posedge reset)
begin
  if (reset)
    aer_request <= 1'b0;
  else if (aer_launch)
    aer_request <= 1'b1;
  else if (ifat_ack)
    aer_request <= 1'b0;
  else 
    aer_request <= aer_request;
end

// AER datum
always @(posedge fifo_readclk or posedge reset)
begin
  if (reset)
    address_event <= 19'h0;
  else
    address_event <= fifo_valid ? fifo_out : address_event;
end

endmodule

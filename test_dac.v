`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN	Lab
// Engineer: Christoph Maier
//
// Create Date:   17:35:17 08/16/2012
// Design Name:   dac
// Module Name:   C:/Xilinx/projects/IFAT_out_fifo_test/test_dac.v
// Project Name:  dac
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: dac
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_dac;

	// Inputs
	reg clk;
	reg [15:0] data;
	reg write;
	reg rack;

	// Outputs
	wire [5:0] address;
	wire read;
	wire busy;
	wire ncs_ld;
	wire sck;
	wire sdo;

	// Instantiate the Unit Under Test (UUT)
	dac uut (
		.clk(clk), 
		.data(data), 
		.write(write), 
		.rack(rack), 
		.address(address), 
		.read(read), 
		.busy(busy), 
		.ncs_ld(ncs_ld), 
		.sck(sck), 
		.sdo(sdo)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		data = 0;
		write = 0;
		rack = 0;
        #221 write= 1;
        #5 write= 0;
        #12000 write= 1;
	end

    always @(posedge clk)
    begin
        rack <= read;
        if (read)
            data <= {4'hD,address,address};
    end

    always
        #2.5 clk= ~clk;
      
endmodule


`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN Lab
// Engineer: Christoph Maier
// 
// Create Date:    17:59:30 01/16/2013 
// Design Name: 
// Module Name:    IFAT_fifo_test_wrapper 
// Project Name:   Neovision2 IFAT
// Target Devices: HiAER-IFAT Level 0 Spartan 6
// Tool versions:  XISE 13.3
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module IFAT_fifo_test_wrapper(
  input gclk_p, gclk_n,       // global board clock
  input smaclk_p, smaclk_n,   // SMA connector clock
  input sysclk_p, sysclk_n,   // system clock (local to FPGA)
  output [5:0] light,         // indicator LEDs
  // SPI to DACs
  output nCS_LD,              // DAC SPI not chip select/load
  output SCK,                 // DAC cereal clock
  output SDO,                 // DAC cereal data
  // IFAT chip mode
  output EXT_HIAER_B,         // IFAT ASIC mode: HiAER external
  output EXT_IFAT_B,          // IFAT ASIC mode: IFAT external
  // IFAT chip master clock
  output IFAT_clk,            // IFAT master clock
  // IFAT chip resets
  output RST_B,               // IFAT AER module reset
  output RESETLFSR_B,         // IFAT event arbitration random generator reset
  output RESETIFAT_B,         // IFAT event arbiter reset

  // IFAT ports
  output [22:0] EXT_IN_TOP,
  output EXT_INREQ_TOP,
  input EXT_INACK_TOP,

  output [22:0] EXT_IN_BOTTOM,
  output EXT_INREQ_BOTTOM,
  input EXT_INACK_BOTTOM,

  input [22:0] EXT_OUT_TOP,
  input EXT_OUTREQ_TOP,
  output EXT_OUTACK_TOP,

  input [22:0] EXT_OUT_BOTTOM,
  input EXT_OUTREQ_BOTTOM,
  output EXT_OUTACK_BOTTOM,

  // USB port
  inout [7:0] DATA_USB,       // USB data
  input RXF_B,                // USB receiver full
  input TXE_B,                // USB transmitter empty
  output RD_B,                // USB read
  output WR                   // USB write
  );

  wire gclk, sysclk, smaclk;  // clock signals
  wire [5:0] A;
  wire [15:0] D;
  wire DS;
  wire dac_write;
  wire busy;

  wire reset;
  wire global_reset_in;

  wire [22:0] aer0, aer1;
  wire aer_req0, aer_req1;
  wire aer_ack0, aer_ack1;

  assign global_reset_in= 0;
  assign RST_B= ~reset;
  assign RESETLFSR_B= ~reset;
  assign RESETIFAT_B= ~reset;
  assign EXT_HIAER_B= 1'b0;
  assign EXT_IFAT_B= 1'b1;

  assign light[5:0]= {~dac_write, busy, DS, nCS_LD, SCK, SDO};

  // assignments of unused outputs
  assign RD_B= 1'b1;
  assign WR= 1'b0;
  assign IFAT_clk= 1'b0;

  IBUFGDS #(.DIFF_TERM("FALSE"), .IOSTANDARD("DEFAULT")) 
    gbuf (.O(gclk), .I(gclk_p), .IB(gclk_n)),
    sysbuf (.O(sysclk), .I(sysclk_p), .IB(sysclk_n)),
    smabuf (.O(smaclk), .I(smaclk_p), .IB(smaclk_n));

  global_reset resetgen(
    .clk(sysclk),
    .rst(global_reset_in),
    .glob_GSR(reset)
  );

  dac dac_control(
    .clk(sysclk),           // master clock
    .data(D),               // parallel input data 
    .write(dac_write),      // write command
    .rack(DS),              // data read acknowlege
    .address(A),            // data address
    .read(AS),              // read data signal
    .busy(busy),            // output indicating state machine is busy
    .ncs_ld(nCS_LD),        // control output to LTC2600: nCS/LD
    .sck(SCK),              // serial clock to LTC2600 daisy chain
    .sdo(SDO)               // serial data out to LTC2600 daisy chain
  );

  div50M trigger(
    .clock(sysclk),
    .q(dac_write)
  );

  rom dacdata(
    .clk(sysclk),
    .A(A),
    .AS(AS),
    .D(D),
    .DS(DS)
   );

	// Digital IFAT port deglitcher
	port_deglitcher 
    port0(
      .clock(sysclk), 
      .reset(reset), 
      .ext_in(EXT_IN_TOP), 
      .ext_inreq(EXT_INREQ_TOP), 
      .ext_inack(EXT_INACK_TOP), 
      .ext_out(EXT_OUT_TOP), 
      .ext_outreq(EXT_OUTREQ_TOP), 
      .ext_outack(EXT_OUTACK_TOP), 
      .aer(aer0), 
      .aer_req(aer_req0), 
      .aer_ack(aer_ack0)
    ),
    port1(
      .clock(sysclk), 
      .reset(reset), 
      .ext_in(EXT_IN_BOTTOM), 
      .ext_inreq(EXT_INREQ_BOTTOM), 
      .ext_inack(EXT_INACK_BOTTOM), 
      .ext_out(EXT_OUT_BOTTOM), 
      .ext_outreq(EXT_OUTREQ_BOTTOM), 
      .ext_outack(EXT_OUTACK_BOTTOM), 
      .aer(aer1), 
      .aer_req(aer_req1), 
      .aer_ack(aer_ack1)
    );

  // very basic handshake and logging for ext_out port
  ext_out_handshake
    ouths0(
      .clk(sysclk),
      .reset(reset),
      .req(EXT_OUTREQ_TOP),
      .ack(EXT_OUTACK_TOP)
    ),
    ouths1(
      .clk(sysclk),
      .reset(reset),
      .req(EXT_OUTREQ_BOTTOM),
      .ack(EXT_OUTACK_BOTTOM)
    );

  // very basic AER generator
  aer_generator
    AERgen0(
      .clk(sysclk),
      .reset(reset),
      .aer(aer0),
      .aer_req(aer_req0),
      .aer_ack(aer_ack0)
    ),
    AERgen1(
      .clk(sysclk),
      .reset(reset),
      .aer(aer1),
      .aer_req(aer_req1),
      .aer_ack(aer_ack1)
    );

endmodule

// basic AER generator
module aer_generator(
  input clk,
  input reset,
  output reg [22:0] aer,
  output reg aer_req,
  input aer_ack
  );

  always @(posedge clk)
  begin
    if (reset)
    begin
      aer <= 23'h0;
      aer_req <= 1'b0;
    end
    else
    begin
      if (aer_ack)
      begin
        aer_req <= 1'b0;
        aer <= aer + 23'h1;
      end
      else
      begin
        aer_req <= 1'b1;
      end
    end
  end

endmodule

// very basic handshake and logging for ext_out port
module ext_out_handshake(
  input clk,
  input reset,
  input req,
  output reg ack
  );

  always @(posedge clk)
  begin
    if (reset)
    begin
      ack <= 1'b0;
    end
    else
    begin
      ack <= req;
    end
  end

endmodule


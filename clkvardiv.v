`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:     UCSD ISN lab 
// Engineer:    Christoph Maier
// 
// Create Date:    22:37:40 11/04/2011 
// Design Name: BFSKtest
// Module Name:    clkvardiv 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: parametric clock divider
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module clkvardiv(divisor, clkin, reset, clkout);
    parameter bits = 8;
    input [bits-1:0] divisor; 
    input clkin;
    input reset;
    output reg clkout;
    reg [bits-1:0] count;
    
    always @(posedge clkin or posedge reset)
    begin
        if (reset)
        begin
            count <= (divisor-(divisor>>1))-1;
            clkout <= 0;
        end
        else
        begin
            if (count == 0)
            begin
                count <=  (clkout? divisor-(divisor>>1) : divisor>>1)-1;
                clkout <= ~clkout;
            end
            else
            begin
                count <= count-1;
                clkout <= clkout;
            end
        end
    end
endmodule

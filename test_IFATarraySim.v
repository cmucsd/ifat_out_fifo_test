`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN	Lab
// Engineer: Christoph Maier
//
// Create Date:   15:47:33 02/19/2013
// Design Name:   IFATarraySim
// Module Name:   C:/HDL/git/IFAT_state_machines/test_IFATarraySim.v
// Project Name:  IFAT_state_machine_test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: IFATarraySim
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_IFATarraySim;

//  reg clk;

	// Inputs
	reg if_ack;

	// Outputs
	wire if_req;
	wire [10:0] if_address;

	// Instantiate the Unit Under Test (UUT)
	IFATarraySim #(12,1,1, 14, 11'h555)uut 
  (
		.address(if_address),
		.req(if_req), 
		.ack(if_ack) 
	);
	initial 
  begin
		// Initialize Inputs
		if_ack <= 0;
//    clk <= 0;
    #2000;
    $stop;
	end

//  always
//  begin
//    #10 clk <= ~clk;
//  end
  always @(posedge if_req)
  begin
    #18;
    $display("Time %d: Address event %h received", $time, if_address);
    if_ack <= 1'b1;
  end

  always @(negedge if_req)
  begin
    #15;
    if_ack <= 1'b0;
  end
endmodule


`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:02:33 03/12/2013
// Design Name:   arbiter_4x4
// Module Name:   C:/HDL/git/IFAT_state_machines/test_arbiter_4x4.v
// Project Name:  IFAT_state_machine_test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: arbiter_4x4
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_arbiter_4x4;

	// Inputs
	reg clk;
	reg reset;
	reg ifat_inreq;
	reg ifat_inack;
	reg ifat_outreq;
	reg ifat_outack;
	reg [3:0] busreq;

	// Outputs
	wire [3:0] busgrant;
	wire [1:0] address;

	// Instantiate the Unit Under Test (UUT)
	arbiter_4x4 uut (
		.clk(clk), 
		.reset(reset), 
		.ifat_inreq(ifat_inreq), 
		.ifat_inack(ifat_inack), 
		.ifat_outreq(ifat_outreq), 
		.ifat_outack(ifat_outack), 
		.busreq(busreq), 
		.busgrant(busgrant), 
		.address(address)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 0;
		ifat_inreq = 0;
		ifat_inack = 0;
		ifat_outreq = 0;
		ifat_outack = 0;
		busreq = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule


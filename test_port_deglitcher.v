`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   19:47:04 04/25/2013
// Design Name:   port_deglitcher
// Module Name:   C:/HDL/git/IFAT_out_fifo_test/test_port_deglitcher.v
// Project Name:  IFAT_out_fifo_test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: port_deglitcher
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_port_deglitcher;

	// Inputs
	reg clock;
	reg reset;
	reg ext_inack;
	reg [22:0] ext_out;
	reg ext_outreq;
	reg ext_outack;
	reg [22:0] aer;
	reg aer_req;

	// Outputs
	wire [22:0] ext_in;
	wire ext_inreq;
	wire aer_ack;

	// Instantiate the Unit Under Test (UUT)
	port_deglitcher uut (
		.clock(clock), 
		.reset(reset), 
		.ext_in(ext_in), 
		.ext_inreq(ext_inreq), 
		.ext_inack(ext_inack), 
		.ext_out(ext_out), 
		.ext_outreq(ext_outreq), 
		.ext_outack(ext_outack), 
		.aer(aer), 
		.aer_req(aer_req), 
		.aer_ack(aer_ack)
	);

	initial begin
		// Initialize Inputs
		clock = 0;
		reset = 0;
		ext_inack = 0;
		ext_out = 0;
		ext_outreq = 0;
		ext_outack = 0;
		aer = 0;
		aer_req = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule


`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN Lab
// Engineer: Christoph Maier
//
// Create Date:   21:58:43 02/21/2013
// Design Name:   ifat_synapse_delay_fifo
// Module Name:   C:/HDL/git/IFAT_out_fifo_test/test_ifat_synapse_delay_fifo.v
// Project Name:  IFAT_out_fifo_test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ifat_synapse_delay_fifo
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_ifat_synapse_delay_fifo;

	// Inputs
	reg clk;
	reg reset;
  reg divreset;
  reg [3:0] top_address;
  reg [18:0] fifo_in;
  reg fifo_write_enable;

	reg ifat_ack;
	reg busgrant;

	// Outputs
	wire [22:0] ifat_in;
	tri0 ifat_req;
	wire busreq;

  // Internal signals
  wire fifo_writeclk;
  wire fifo_full;

	// Instantiate the Unit Under Test (UUT)
	ifat_synapse_delay_fifo uut (
		.top_address(top_address), 
		.reset(reset), 
		.inclk(fifo_writeclk), 
		.full(fifo_full), 
		.data_in(fifo_in), 
		.write_enable(fifo_write_enable), 
		.outclk(clk), 
		.ifat_in(ifat_in), 
		.ifat_req(ifat_req), 
		.ifat_ack(ifat_ack), 
		.busreq(busreq), 
		.busgrant(busgrant)
	);

  // FIFO input clock divider
  clkvardiv #(6) fifo_wr_clk_div
  (
      .divisor(8),
      .clkin(clk), 
      .reset(reset), 
      .clkout(fifo_writeclk)
  );


	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 1;
    divreset = 1;
    top_address = 9;
    fifo_write_enable = 1;
		ifat_ack = 0;
		busgrant = 0;
    #20 divreset = 0;
		#380 reset= 0;
    #49900 $stop;
	end

// FIFO in event generator
  always @(negedge fifo_writeclk or reset)
  begin
    if (reset)
      fifo_in <= 19'h0F;
    else
      fifo_in <= fifo_in + 19'h10;
  end

// clock generator
  always
  begin
    clk = #10 ~clk;
  end

// extremaly basic arbiter
  always @(posedge clk)
  begin
    busgrant <= busreq;
  end

// extreamely basic handshake
  always @(posedge clk)
  begin
    ifat_ack <= ifat_req;
  end

endmodule

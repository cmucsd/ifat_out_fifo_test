`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN Lab
// Engineer: Christoph Maier
// 
// Create Date:    14:49:41 03/12/2013 
// Design Name: 
// Module Name:    arbiter_4x4 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Arbiter for 4 groups of 4 quad_synapse_delay_fifo blocks
//      that deliver address events to the IFAT synaptic inputs.
//    To a first approximation, this is a round-robin arbiter.
//    Tricky part: The address on this level selects the neural outputs, too,
//      so each address needs to be applied to the bus even if no synaptic input is present,
//      and the address must not change while an output address event is being transmitted.
//      I'll solve this by letting arbiter_4x4 take control 
//      over the 2 most significant bits of IFAT ext_in.
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module arbiter_4x4(
  input clk,
  input reset,
  input ifat_inreq,
  input ifat_inack,
  input ifat_outreq,
  input ifat_outack,
  input [3:0] busreq,
  output reg [3:0] busgrant,
  output reg [1:0] address
  );

  parameter [1:0]
    IDLE = 2'h0,
    GRANT = 2'h1,
    NEXT = 2'h3;

  reg [1:0] state;
  wire [3:0] token;

  wire req; // multiplexed request signal
  wire buses_active;  // ok to switch bus control

  assign req = |(busreq & token);
  assign buses_active = |{ifat_outreq, ifat_outack, ifat_inreq, ifat_inack};

  // token ring [ http://search.dilbert.com/comic/Token%20Ring ]
  genvar block;
  generate for (block = 0; block < 4; block = block + 1)
    begin:ASSIGNMENTS
      assign token[block] = (block == address);
    end
  endgenerate

  always @(posedge clk or posedge reset)
  begin
    if (reset)
      state <= IDLE;
    else
    case (state)
    IDLE:
      state <= req ? GRANT : NEXT;
    GRANT:
      state <= (req | buses_active) ? GRANT : NEXT;
    NEXT:
      state <= buses_active ? NEXT : IDLE;
    default:
      state <= IDLE;
    endcase
  end

  // address Gray counter
  always @(posedge clk or posedge reset)
  begin
    if (reset)
      address <= 2'h0;
    else
    begin
      if ((NEXT == state) && ~buses_active)
      begin
        case (address)
        2'h0:
          address <= 2'h1;
        2'h1:
          address <= 2'h3;
        2'h3:
          address <= 2'h2;
        2'h2:
          address <= 2'h0;
        default:
          address <= 2'h0;
        endcase
      end
      else
        address <= address;
    end
  end

  wire granted;
  assign granted = (GRANT == state);

  always @(posedge clk or posedge reset)
  begin
    if (reset)
    begin
      busgrant <= 4'h0;
    end
    else
    begin
      busgrant <= busreq & token & {4{granted}};
    end
  end

endmodule

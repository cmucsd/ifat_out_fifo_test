`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN Lab
// Engineer: Christoph Maier
// 
// Create Date:    13:05:46 03/01/2013 
// Design Name: 
// Module Name:    arbiter_quad 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Arbiter for an array of 4 ifat_synapse_delay_fifo elements
//
// Dependencies:
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module arbiter_quad(
  input clk,
  input reset,

  output reg busreq_master,
  input busgrant_master,

  input [3:0] busreq_slave,
  output reg [3:0] busgrant_slave
  );

  parameter [1:0]
    IDLE = 2'h0,
    REQ = 2'h1,
    GRANT = 2'h3,
    NEXT = 2'h2;

  reg [1:0] state;
  reg [3:0] token;
  wire req;

  assign req = |(busreq_slave & token);

  always @(posedge clk or posedge reset)
  begin
    if (reset)
      state <= IDLE;
    else
    case (state)
    IDLE:
      state <= req ? REQ : NEXT;
    REQ: 
      state <= busgrant_master ? GRANT : REQ;
    GRANT:
      state <= req ? GRANT : NEXT;
    NEXT:
      state <= IDLE;
    default:
      state <= IDLE;
    endcase
  end

  wire granted;
  assign granted = (GRANT == state);

  always @(posedge clk or posedge reset)
  begin
    if (reset)
    begin
      busreq_master <= 1'b0;
      busgrant_slave <= 4'h0;
    end
    else
    begin
      busreq_master <= (REQ == state || granted);
      busgrant_slave <= busreq_slave & token & {4{granted}};
    end
  end

  // token ring [ http://search.dilbert.com/comic/Token%20Ring ]
  always @(posedge clk or posedge reset)
  begin
    if (reset)
      token <= 4'h1;
    else
      token <= (NEXT == state) ? {token[2:0], token[3]} : token;
  end

endmodule

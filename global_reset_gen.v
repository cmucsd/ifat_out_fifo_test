`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:03:47 07/18/2012 
// Design Name: 
// Module Name:    global_reset_gen 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module global_reset(
	input clk,
	input rst,
	
    output reg glob_GSR
    );


	 //internal register
	 reg [7:0] ps_cnt;

    wire ps_cnt_clr;


	///////////////////////////////
	// clock cycle will be 5.12ns
	///////////////////////////////
	
   ////////////////////////
   // Prescale 
   // 
	
	always @(posedge clk or posedge rst)
	if(rst)
		ps_cnt <= 8'b0;
	else if(~ps_cnt_clr)
		ps_cnt <= 8'd72;
	else if(ps_cnt_clr)
		ps_cnt <= ps_cnt + 8'b1;
	
	//count 200
	assign ps_cnt_clr = ~(ps_cnt == 8'd72);

	always @(posedge clk or posedge rst)
	if(rst)
		glob_GSR <= 1'b1;
	else
		glob_GSR <= ps_cnt_clr;
		

endmodule

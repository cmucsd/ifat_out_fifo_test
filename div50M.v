`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN Lab
// Engineer: Christoph Maier
// 
// Create Date:    15:53:44 02/08/2013 
// Design Name: 
// Module Name:    div50M 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module div50M(
	input clock,
	output q
	);

	reg[25:0] count;
	reg outbit;

	assign q=outbit;
	always @(posedge clock)
	begin
		if (count == 0)
		begin
			count <= 999_999;
			outbit <= 1;
		end
		else
        begin
			count <= count - 26'b1;
            outbit <= 0;
        end
	end
endmodule

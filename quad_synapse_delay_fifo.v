`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN Lab
// Engineer: Christoph Maier
// 
// Create Date:    16:35:46 03/01/2013 
// Design Name: 
// Module Name:    quad_synapse_delay_fifo 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: block of 4 delay IFAT delay FIFOs.
//
// Dependencies: ifat_synapse_delay_fifo.v, arbiter_quad.v
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module quad_synapse_delay_fifo(
  input reset,
  input inclk,
  output full,
  input [1:0] top_address,
  input [22:0] data_in,
  input write_enable,
  input outclk,
  output [22:0] ifat_in,
  output ifat_req,
  input ifat_ack,
  output busreq,
  input busgrant
  );

  wire [22:0] to_ifat [3:0];
  wire [3:0] to_ifat_req;

  wire selected;  // block is selected
  wire [3:0] writen;  // delay FIFO write enable
  wire [3:0] req;     // delay FIFO bus requests
  wire [3:0] grant;   // delay FIFO bus grants
  wire [3:0] fifo_full; // delay FIFO full, raw signals
  wire [3:0] full_flags;  // delay FIFO full, enabled by address for multiplexing

  assign selected = top_address == data_in[22:21];
  assign full = selected ? |(full_flags) : 1'b0;

  genvar block;
  generate for (block = 0; block < 4; block = block + 1)
    begin:ASSIGNMENTS
      assign writen[block] = write_enable & selected & (block == data_in[20:19]);
      assign full_flags[block] = fifo_full[block] & (block == data_in[20:19]);
    end
  endgenerate

  assign ifat_in = to_ifat[0] | to_ifat[1] | to_ifat[2] | to_ifat[3];
  assign ifat_req = |(to_ifat_req);

  // Synapse delay FIFO
  ifat_synapse_delay_fifo 
    fifo0(
      .top_address({top_address,2'h0}), 
      .reset(reset), 
      .inclk(inclk), 
      .full(fifo_full[0]), 
      .data_in(data_in[18:0]), 
      .write_enable(writen[0]), 
      .outclk(outclk),
      .ifat_in(to_ifat[0]),
      .ifat_req(to_ifat_req[0]),
      .ifat_ack(ifat_ack),
      .busreq(req[0]), 
      .busgrant(grant[0])
    ),
    fifo1(
      .top_address({top_address,2'h1}), 
      .reset(reset), 
      .inclk(inclk), 
      .full(fifo_full[1]), 
      .data_in(data_in[18:0]), 
      .write_enable(writen[1]), 
      .outclk(outclk), 
      .ifat_in(to_ifat[1]),
      .ifat_req(to_ifat_req[1]),
      .ifat_ack(ifat_ack),
      .busreq(req[1]), 
      .busgrant(grant[1])
    ),
    fifo2(
      .top_address({top_address,2'h2}), 
      .reset(reset), 
      .inclk(inclk), 
      .full(fifo_full[2]), 
      .data_in(data_in[18:0]), 
      .write_enable(writen[2]), 
      .outclk(outclk), 
      .ifat_in(to_ifat[2]),
      .ifat_req(to_ifat_req[2]),
      .ifat_ack(ifat_ack),
      .busreq(req[2]), 
      .busgrant(grant[2])
    ),
    fifo3(
      .top_address({top_address,2'h3}), 
      .reset(reset), 
      .inclk(inclk), 
      .full(fifo_full[3]), 
      .data_in(data_in[18:0]), 
      .write_enable(writen[3]), 
      .outclk(outclk), 
      .ifat_in(to_ifat[3]),
      .ifat_req(to_ifat_req[3]),
      .ifat_ack(ifat_ack),
      .busreq(req[3]), 
      .busgrant(grant[3])
    );

  arbiter_quad 
    arbiter(
      .clk(outclk),
      .reset(reset),
      .busreq_master(busreq),
      .busgrant_master(busgrant),
      .busreq_slave(req),
      .busgrant_slave(grant)
    );

endmodule

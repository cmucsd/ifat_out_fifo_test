`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN Lab
// Engineer: Christoph Maier
//
// Create Date:   20:26:00 02/20/2013
// Design Name:   ifat_synapse_scheduler
// Module Name:   C:/HDL/git/IFAT_out_fifo_test/test_ifat_synapse_scheduler.v
// Project Name:  IFAT_out_fifo_test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ifat_synapse_scheduler
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_ifat_synapse_scheduler;

	// Inputs
	reg clk;
	reg reset;
  reg divreset;
  reg [3:0] top_address;
  reg [18:0] fifo_in;
  reg fifo_write_enable;

	reg ifat_ack;
	reg busgrant;

	// Outputs
	wire fifo_readclk;
	wire fifo_read;
	wire [22:0] ifat_in;
	tri0 ifat_req;
	wire busreq;

  // Internal signals
  wire fifo_writeclk;
  wire fifo_full;

	wire [18:0] fifo_out;
	wire fifo_valid;
  wire fifo_empty;

	// Instantiate the Unit Under Test (UUT)
	ifat_synapse_scheduler scheduler (
		.clk(clk), 
		.reset(reset),
    .top_address(top_address),
		.fifo_out(fifo_out), 
		.fifo_readclk(fifo_readclk), 
		.fifo_empty(fifo_empty),
    .fifo_valid(fifo_valid),
		.fifo_read(fifo_read), 
		.ifat_in(ifat_in), 
		.ifat_req(ifat_req), 
		.ifat_ack(ifat_ack), 
		.busreq(busreq), 
		.busgrant(busgrant)
	);

  FIFO19x16 synapse_fifo (
    .rst(reset), // input
    .full(fifo_full), // output full
    .wr_clk(fifo_writeclk), // input wr_clk
    .wr_en(fifo_write_enable), // input wr_en
    .din(fifo_in), // input [18 : 0] din
    .empty(fifo_empty), // output empty
    .valid(fifo_valid), // output valid
    .rd_clk(fifo_readclk), // input rd_clk
    .rd_en(fifo_read), // input rd_en
    .dout(fifo_out) // output [18 : 0] dout
  );

  // FIFO input clock divider
  clkvardiv #(6) fifo_wr_clk_div
  (
      .divisor(8),
      .clkin(clk), 
      .reset(divreset), 
      .clkout(fifo_writeclk)
  );


	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 1;
    divreset = 1;
    top_address = 9;
    fifo_write_enable = 1;
		ifat_ack = 0;
		busgrant = 0;
    #20 divreset = 0;
		#380 reset= 0;
    #49900 $stop;
	end

// FIFO in event generator
  always @(negedge fifo_writeclk or reset)
  begin
    if (reset)
      fifo_in <= 19'h0F;
    else
      fifo_in <= fifo_in + 19'h10;
  end

// clock generator
  always
  begin
    clk = #10 ~clk;
  end

// extremaly basic arbiter
  always @(posedge clk)
  begin
    busgrant <= busreq;
  end

// extreamely basic handshake
  always @(posedge clk)
  begin
    ifat_ack <= ifat_req;
  end

endmodule

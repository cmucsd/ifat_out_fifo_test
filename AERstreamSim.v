`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN	Lab
// Engineer: Christoph Maier
// 
// Create Date:    10:58:28 02/20/2013 
// Design Name: 
// Module Name:    AERstreamSim 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Simulate a train of address events
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module AERstreamSim(
  output [31:0] AddressEvent,
  output reg request,
  input acknowledge,
  input clock,  // optional clock input
  input reset   // optional reset input
  );
parameter delay = 200, setup = 2, hold = 2, mode = 0, seed = 0, increment = 1, pulsewidth = 63;

wire [16:0] address;
reg [16:0] count;
reg [5:0] weight;

assign AddressEvent = {9'b0, address, weight};

genvar i;
generate for(i=0; i<17; i=i+1)
  begin:BITREVERSE  // make bitreverse optional, depending on mode
    assign address[i] = mode ? count[16-i] : count[i];
  end
endgenerate


initial
begin
  count <= seed;
  weight <= pulsewidth;
end

always @(posedge acknowledge)
begin
  #(hold);
  request <= 1'b0;
end

always @(negedge acknowledge)
begin
  #(delay);
  count <= count + increment;
  #(setup);
  request <= 1'b1;
    $display("Time %d S G %h %h: Address event %h, address %h, weight %d generated to be sent to IFAT synapses", $time, address, weight, AddressEvent, address, weight);
end

endmodule

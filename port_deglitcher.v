`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:35:00 04/25/2013 
// Design Name: 
// Module Name:    port_deglitcher
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Module containing the interface to one IFAT digital port
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module port_deglitcher(
  input clock,
  input reset,
// events into the synapses
  output [22:0] ext_in,
  output ext_inreq,
  input ext_inack,
// events from the neurons
  input [22:0] ext_out,
  input ext_outreq,
  input ext_outack,
// internal: events into synapses
  input [22:0] aer,
  input aer_req,
  output reg aer_ack
  );

wire [22:0] ext_in_or;
wire [22:0] ext_in_bus [3:0];
wire [3:0] ext_inreq_bus;

wire [1:0] address;

reg [22:0] fifo_in;
wire [3:0] fifo_full;
reg fifo_write_enable;

wire [3:0] busreq;
wire [3:0] busgrant;

wire inclk;
wire outclk;

assign ext_in_or = ext_in_bus[0] | ext_in_bus[1] | ext_in_bus[2] | ext_in_bus[3];
assign ext_in = {address, ext_in_or[20:0]};
assign ext_inreq = |(ext_inreq_bus);

assign inclk = clock;
assign outclk = clock;

// Quad synapse delay FIFO blocks
quad_synapse_delay_fifo
  block0(
    .reset(reset), 
    .inclk(inclk), 
    .full(fifo_full[0]), 
    .top_address(2'h0), 
    .data_in(fifo_in), 
    .write_enable(fifo_write_enable), 
    .outclk(outclk), 
    .ifat_in(ext_in_bus[0]),
    .ifat_req(ext_inreq_bus[0]), 
    .ifat_ack(ext_inack), 
    .busreq(busreq[0]), 
    .busgrant(busgrant[0])
  ),
  block1(
    .reset(reset), 
    .inclk(inclk), 
    .full(fifo_full[1]), 
    .top_address(2'h1), 
    .data_in(fifo_in), 
    .write_enable(fifo_write_enable), 
    .outclk(outclk), 
    .ifat_in(ext_in_bus[1]),
    .ifat_req(ext_inreq_bus[1]), 
    .ifat_ack(ext_inack), 
    .busreq(busreq[1]), 
    .busgrant(busgrant[1])
  ),
  block2(
    .reset(reset), 
    .inclk(inclk), 
    .full(fifo_full[2]), 
    .top_address(2'h2), 
    .data_in(fifo_in), 
    .write_enable(fifo_write_enable), 
    .outclk(outclk), 
    .ifat_in(ext_in_bus[2]),
    .ifat_req(ext_inreq_bus[2]), 
    .ifat_ack(ext_inack), 
    .busreq(busreq[2]), 
    .busgrant(busgrant[2])
  ),
  block3(
    .reset(reset), 
    .inclk(inclk), 
    .full(fifo_full[3]), 
    .top_address(2'h3), 
    .data_in(fifo_in), 
    .write_enable(fifo_write_enable), 
    .outclk(outclk), 
    .ifat_in(ext_in_bus[3]),
    .ifat_req(ext_inreq_bus[3]), 
    .ifat_ack(ext_inack), 
    .busreq(busreq[3]), 
    .busgrant(busgrant[3])
  );

// arbiter
	arbiter_4x4 arbiter(
		.clk(outclk), 
		.reset(reset), 
		.ifat_inreq(ext_inreq_bus), 
		.ifat_inack(ext_inack), 
		.ifat_outreq(ext_outreq), 
		.ifat_outack(ext_outack), 
		.busreq(busreq), 
		.busgrant(busgrant), 
		.address(address)
	);

wire new_aer;
assign new_aer = aer_req & ~aer_ack;
assign aer_done = ~aer_req & aer_ack;

// synchronize generated AER with FIFO
always @(posedge clock)
begin
  if (reset)
  begin
    fifo_in <= 23'h0;
    fifo_write_enable <= 1'b0;
    aer_ack <= 1'b0;
  end
  else
  begin
    fifo_write_enable <= |(fifo_full) ? 1'b0 : new_aer;
    fifo_in <= new_aer ? aer[22:0] : fifo_in;
    if (new_aer)
      aer_ack <= 1'b1;
    else if (aer_done)
      aer_ack <= 1'b0;
    else
      aer_ack <= aer_ack;
  end
end

endmodule

`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:14:53 03/01/2013
// Design Name:   quad_synapse_delay_fifo
// Module Name:   C:/HDL/git/IFAT_state_machines/test_quad_synapse_delay_fifo.v
// Project Name:  IFAT_state_machine_test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: quad_synapse_delay_fifo
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_quad_synapse_delay_fifo;

	// Inputs
	reg reset;
	wire inclk;
	wire [1:0] top_address;
	wire [31:0] data_in;
	wire write_enable;
	reg outclk;
	reg ifat_ack;
	reg busgrant;

	// Outputs
	tri0 full;
  tri0 [22:0] ifat_in;
	tri0 ifat_req;
	wire busreq;

  // internal
  assign top_address = 2'b0;
  wire request;
  wire acknowledge;

	// Instantiate the Unit Under Test (UUT)
	quad_synapse_delay_fifo uut (
		.reset(reset), 
		.inclk(inclk), 
		.full(full), 
		.top_address(top_address), 
		.data_in(data_in), 
		.write_enable(write_enable), 
		.outclk(outclk), 
    .ifat_in(ifat_in),
		.ifat_req(ifat_req), 
		.ifat_ack(ifat_ack), 
		.busreq(busreq), 
		.busgrant(busgrant)
	);

	// Generate AER stream
	AERstreamSim #(7, 1, 1, 0, 0, 8195, 12) 
  aer_stream(
		.AddressEvent(data_in), 
		.request(request), 
		.acknowledge(acknowledge), 
		.clock(inclk), 
		.reset(reset)
	);

  // FIFO input handshake
  parameter [1:0]
    IDLE = 2'h0,
    SEND = 2'h1,
    ACK = 2'h3;
  reg [1:0] fifo_in_state;

  always @(posedge inclk or posedge reset)
  begin
    if (reset)
    begin
      fifo_in_state <= IDLE;
    end
    else
    begin
      case (fifo_in_state)
      IDLE:
        fifo_in_state <= (request & ~full) ? SEND : IDLE;
      SEND:
        fifo_in_state <= ACK;
      ACK:
        fifo_in_state <= request? ACK : IDLE;
      default:
        fifo_in_state <= IDLE;
      endcase
    end
  end

  assign write_enable = (SEND == fifo_in_state);
  assign acknowledge = (ACK == fifo_in_state);

  // FIFO input clock divider
  clkvardiv #(6) fifo_wr_clk_div
  (
      .divisor(2),
      .clkin(outclk), 
      .reset(reset), 
      .clkout()
  );
  assign inclk = outclk;

	initial begin
		// Initialize Inputs
		reset = 1;
		outclk = 0;
		busgrant = 0;
		#100 reset= 0;
    #49900 $stop;
	end

// clock generator
  always
  begin
    outclk = #10 ~outclk;
  end

// extremaly basic arbiter
  always @(posedge outclk)
  begin
    busgrant <= busreq;
  end

// extreamely basic handshake
  always @(posedge outclk)
  begin
    ifat_ack <= ifat_req;
  end
      
endmodule
